var app = angular.module('pruebaApp',['ngRoute','pruebaApp_servicios','pruebaApp.listadoCntrol','pruebaApp.detalleCntrol','pruebaApp.gestionCntrol']);

app.controller('mainCtrl', ['$scope','$http','listadoPeliculas','agregarPelicula', function($scope,$http,listadoPeliculas,agregarPelicula){
  
	$scope.menuSuperior = 'paginas/menu_superior.html';
	
	$scope.datos_autenticacion={
		user:"pelicula",
		pass:"pelicula2017*"
		
	};

	$scope.datos_login={
		
	};

	$scope.datos_insertar={};
	$scope.loggin=false,



	$scope.loguearse=function()
	{
		console.log($scope.datos_login);
		console.log($scope.datos_autenticacion);
		if($scope.datos_login.username==$scope.datos_autenticacion.user && $scope.datos_login.password==$scope.datos_autenticacion.pass)
		{
			console.log("si");
			$scope.loggin=true;

		}
		

	}

	

	
}]);

//RUTAS
app.config( ['$routeProvider',function($routeProvider){ 

	$routeProvider
		.when('/',{
			templateUrl: 'paginas/inicio.html'
			//controller: 'inicioCtrl'
		})
		.when('/login',{
			templateUrl: 'paginas/login.html',
			controller: 'mainCtrl'
		})
		.when('/listado',{
			templateUrl: 'paginas/listado.html',
			controller: 'listadoCntrol'
		})
		.when('/detalle/:id',{
			templateUrl: 'paginas/detalle.html',
			controller: 'detalleCntrol'
		})		
		.when('/gestion',{
			templateUrl: 'paginas/gestion.html',
			controller: 'gestionCntrol'
		})		
		.otherwise({
			redirectTo: '/'
		})


}]);