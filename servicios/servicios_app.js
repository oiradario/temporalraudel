var app=angular.module('pruebaApp_servicios', []);

app.factory('listadoPeliculas',['$http','$q',function($http,$q){
       var listado_pelis = {};
       
       var result={

            

                obtenerPeliculas: function( ) {
                var d=$q.defer();
                $http.get('https://api.themoviedb.org/3/discover/movie?api_key=e43c483569b04a82486e7edda4f72d22').success(function(data)
                    {
                       
                        listado_pelis=data;
                        d.resolve();
                        return listado_pelis;                
                    })
                    .error(function(){

                        d.reject();
                        console.error("Errorrrrrrr");
                                
                    })
                     return d.promise;
              },
              getListado:function()
              {
                return listado_pelis;

              }
                
       }; 
        return result;
       
    }]);


app.factory('detallePeliServicio',['$http','$q',function($http,$q){
        
        var detalle_peli={};
        var result = {
        
        		   
            detalle: function(id_detalle) {
                
                var d=$q.defer();

                $http.get('https://api.themoviedb.org/3/movie/'+id_detalle+'?api_key=e43c483569b04a82486e7edda4f72d22')
			 	.success(function(data)
			 	{
                 
                 	detalle_peli=data;
			 		d.resolve();
					
                	return detalle_peli;
 		

			 	}).error(function(){

			 		
			 		d.reject();
			 		console.error("Errorrrrrrr");
					 		
			 	})
			 	return d.promise;
            },
            getDetalle:function()
            {

                return detalle_peli;
            }
            
            

        }
     
        return result;
    }]);



app.factory('agregarPelicula',['$q',function($q){
        
        var pelicula=[];
        var result = {
        
                   
            agregar: function(new_pelicula) {
                
                var d=$q.defer();
                if(new_pelicula)
                {
                    pelicula.push(new_pelicula);
                d.resolve();
                return pelicula;    
                }
                
                return d.promise;
            },
            getListapelicula:function()
            {

                return pelicula;
            }
 

        }
     
        return result;
    }]);



app.factory('eliminarPelicula',['$q',function($q){
        
        var pelicula=[];
        var result = {
        
                   
            eliminar: function(pos,lista) {
                
                var d=$q.defer();
                if(pos && lista)
                {
                    lista.splice(pos);
                    pelicula=lista;
                d.resolve();
                return pelicula;    
                }
                
                return d.promise;
            },
            getListapelicula:function()
            {

                return pelicula;
            }
 

        }
     
        return result;
    }]);
